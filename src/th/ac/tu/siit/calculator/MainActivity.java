package th.ac.tu.siit.calculator;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button b0 = (Button) findViewById(R.id.num0);
		b0.setOnClickListener(this);
		Button b1 = (Button) findViewById(R.id.num1);
		b1.setOnClickListener(this);
		Button b2 = (Button) findViewById(R.id.num2);
		b2.setOnClickListener(this);
		Button b3 = (Button) findViewById(R.id.num3);
		b3.setOnClickListener(this);
		Button b4 = (Button) findViewById(R.id.num4);
		b4.setOnClickListener(this);
		Button b5 = (Button) findViewById(R.id.num5);
		b5.setOnClickListener(this);
		Button b6 = (Button) findViewById(R.id.num6);
		b6.setOnClickListener(this);
		Button b7 = (Button) findViewById(R.id.num7);
		b7.setOnClickListener(this);
		Button b8 = (Button) findViewById(R.id.num8);
		b8.setOnClickListener(this);
		Button b9 = (Button) findViewById(R.id.num9);
		b9.setOnClickListener(this);

		((Button) findViewById(R.id.add)).setOnClickListener(this);
		((Button) findViewById(R.id.sub)).setOnClickListener(this);
		((Button) findViewById(R.id.mul)).setOnClickListener(this);
		((Button) findViewById(R.id.div)).setOnClickListener(this);

		((Button) findViewById(R.id.ac)).setOnClickListener(this);
		((Button) findViewById(R.id.bs)).setOnClickListener(this);

		((Button) findViewById(R.id.dot)).setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// String output = "";
	double value1 = 0.0;
	String operation = "";
	String output = "0";

	@Override
	public void onClick(View v) {
		int id = v.getId();
		TextView outputTV = (TextView) findViewById(R.id.output);
		// = outputTV.getText().toString();

		switch (id) {
		case R.id.num0:
		case R.id.num1:
		case R.id.num2:
		case R.id.num3:
		case R.id.num4:
		case R.id.num5:
		case R.id.num6:
		case R.id.num7:
		case R.id.num8:
		case R.id.num9:
			output = output + ((Button) v).getText().toString();
			outputTV.setText(output);
			break;
		case R.id.add:

			if (operation.equals("")) {

				value1 = Double.parseDouble(output);
				operation = "+";

				output = "0";
			} else {
				// /////
				double value2 = Double.parseDouble(output);
				if (operation.equals("+"))
					value1 = value1 + value2;
				else if (operation.equals("-"))
					value1 = value1 - value2;
				else if (operation.equals("*"))
					value1 = value1 * value2;
				else if (operation.equals("/"))
					value1 = value1 / value2;

				output = String.valueOf(value1);
				outputTV.setText(output);
				// /////

				value1 = Double.parseDouble(output);
				operation = "+";

				output = "0";

			}

			// outputTV.setText(output);
			break;

		case R.id.sub:

			if (operation.equals("")) {

				value1 = Double.parseDouble(output);
				operation = "-";

				output = "0";
			} else {
				// /////
				double value2 = Double.parseDouble(output);
				if (operation.equals("+"))
					value1 = value1 + value2;
				else if (operation.equals("-"))
					value1 = value1 - value2;
				else if (operation.equals("*"))
					value1 = value1 * value2;
				else if (operation.equals("/"))
					value1 = value1 / value2;

				output = String.valueOf(value1);
				outputTV.setText(output);
				// /////

				value1 = Double.parseDouble(output);
				operation = "-";

				output = "0";
				break;
			}
		case R.id.mul:

			if (operation.equals("")) {

				value1 = Double.parseDouble(output);
				operation = "*";

				output = "0";
			} else {
				// /////
				double value2 = Double.parseDouble(output);
				if (operation.equals("+"))
					value1 = value1 + value2;
				else if (operation.equals("-"))
					value1 = value1 - value2;
				else if (operation.equals("*"))
					value1 = value1 * value2;
				else if (operation.equals("/"))
					value1 = value1 / value2;

				output = String.valueOf(value1);
				outputTV.setText(output);
				// /////

				value1 = Double.parseDouble(output);
				operation = "*";

				output = "0";
				break;

			}
		case R.id.div:

			if (operation.equals("")) {

				value1 = Double.parseDouble(output);
				operation = "/";

				output = "0";
			} else {
				// /////
				double value2 = Double.parseDouble(output);
				if (operation.equals("+"))
					value1 = value1 + value2;
				else if (operation.equals("-"))
					value1 = value1 - value2;
				else if (operation.equals("*"))
					value1 = value1 * value2;
				else if (operation.equals("/"))
					value1 = value1 / value2;

				output = String.valueOf(value1);
				outputTV.setText(output);
				// /////

				value1 = Double.parseDouble(output);
				operation = "/";

				output = "0";

			}

			// outputTV.setText(output);
			break;

		case R.id.ac:
			value1 = 0.0;
			output = "0";
			operation = "";
			outputTV.setText("0");
			break;

		case R.id.bs:
			int l = output.length();
			if (l > 1) {
				output = output.substring(0, output.length() - 1);
			} else {
				output = "0";

			}
			outputTV.setText(output);
			break;

		case R.id.equ:

			//output = "0";
			if (operation.equals("+")) {
				double value2 = Double.parseDouble(output);
				double res = value1 + value2;
				output = String.valueOf(res);
				outputTV.setText(output);
			} else {

			}

		}

	}

}
